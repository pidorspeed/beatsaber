﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlotController : MonoBehaviour
{

    float MIN_BEAT_SEPARATION=0.22f;
    float tIni; 

   
    void Start()
    {
        tIni = Time.time;  
    }

    public void updatePlot(List<SpectralFluxInfo> pointInfo,  int curIndex=-1 )
    {
        
        int i = curIndex;
        
        if (pointInfo[i].isPeak)
        {
            if (Time.time - tIni >= MIN_BEAT_SEPARATION)
            {
                setPointHeight();
                tIni = Time.time;
            }
        }
       

    }


    void setPointHeight()
    {
        
            GameObject[] g = GetComponent<Spawn>().cubes;
            Transform[] t = GetComponent<Spawn>().points;
            GameObject cube = Instantiate(g[Random.Range(0, 2)], t[Random.Range(0, 3)]);
            cube.transform.localPosition = Vector3.zero;
        
        

    }
}

