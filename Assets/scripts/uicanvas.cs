﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uicanvas : MonoBehaviour
{
     GameObject delete;
    public GameObject text;
    public GameObject[] lifes;
    AudioSource spawnsourse;
    AudioSource audiosoure;
    private void Start()
    {
        spawnsourse = GameObject.Find("Spawn").GetComponent<AudioSource>();
        audiosoure = GameObject.Find("Main Camera").GetComponent<AudioSource>();
        delete= GameObject.Find("spawnbolls");
    }

    public void foldcounter(int c)
    {
        if (c < lifes.Length)
        {
            lifes[c].GetComponent<Image>().color = Color.black;
        }
        else {
            text.SetActive(true);
            spawnsourse.enabled = false;
            audiosoure.enabled = false;
            delete.SetActive(false);
        }
        
    }
}
