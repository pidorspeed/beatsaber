﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectralFluxInfo {
	public float time;
	public float spectralFlux;
	public float threshold;
	public float prunedSpectralFlux;
	public bool isPeak;
}

public class SpectralFluxAnalyzer {
	int numSamples = 1024;


	// Множитель чувствительности среднего порога.
	// В этом случае, если образец выпрямленного спектрального потока в 1,5 раза больше среднего, это бит
	float thresholdMultiplier = 1.3f;

	
   // Количество образцов для усреднения в нашем окне
	int thresholdWindowSize = 50;

	public List<SpectralFluxInfo> spectralFluxSamples;

	float[] curSpectrum;
	float[] prevSpectrum;

	int indexToProcess;

	public SpectralFluxAnalyzer () {
		spectralFluxSamples = new List<SpectralFluxInfo> ();

		// Начинаем обработку с середины первого окна и увеличиваем на 1 
		indexToProcess = thresholdWindowSize / 2;

		curSpectrum = new float[numSamples];
		prevSpectrum = new float[numSamples];
	}

	public void setCurSpectrum(float[] spectrum) {
		curSpectrum.CopyTo (prevSpectrum, 0);
		spectrum.CopyTo (curSpectrum, 0);
	}
		
	public void analyzeSpectrum(float[] spectrum, float time) {
		// Установить спектр
		setCurSpectrum(spectrum);

		//текущий спектральный поток из спектра
		SpectralFluxInfo curInfo = new SpectralFluxInfo();
		curInfo.time = time;
		curInfo.spectralFlux = calculateRectifiedSpectralFlux ();
		spectralFluxSamples.Add (curInfo);

		//у нас достаточно образцов, чтобы обнаружить пик ?
		if (spectralFluxSamples.Count >= thresholdWindowSize) {
			// Получаем порог потока для временного окна, окружающего индекс, для обработки
			spectralFluxSamples[indexToProcess].threshold = getFluxThreshold (indexToProcess);

			// Сохраняем только количество 
			spectralFluxSamples[indexToProcess].prunedSpectralFlux = getPrunedSpectralFlux(indexToProcess);

			/// Теперь, когда  n,имеет соседей  для определения пика
			int indexToDetectPeak = indexToProcess - 1;

			bool curPeak = isPeak (indexToDetectPeak);

			if (curPeak) {
				spectralFluxSamples [indexToDetectPeak].isPeak = true;
			}
			indexToProcess++;
		}
		else {
			Debug.Log(string.Format("Not ready yet.  At spectral flux sample size of {0} growing to {1}", spectralFluxSamples.Count, thresholdWindowSize));
		}
	}

	float calculateRectifiedSpectralFlux() {
		float sum = 0f;

		// 2 в данном случе это чанк кот соответствует частоте 2*47гц
		for (int i = 0; i < 3; i++) {//numSamples
			sum += Mathf.Max (0f, curSpectrum [i] - prevSpectrum [i]);
		}
		return sum;
	}

	float getFluxThreshold(int spectralFluxIndex) {
		// Сколько образцов в прошлом и будущем мы включили в наше среднее
		int windowStartIndex = Mathf.Max (0, spectralFluxIndex - thresholdWindowSize / 2);
		int windowEndIndex = Mathf.Min (spectralFluxSamples.Count - 1, spectralFluxIndex + thresholdWindowSize / 2);

		// Добавляем наш спектральный поток 
		float sum = 0f;
		for (int i = windowStartIndex; i < windowEndIndex; i++) {
			sum += spectralFluxSamples [i].spectralFlux;
		}

		//Возвращаем среднее значение, умноженное на наш множитель чувствительности
		float avg = sum / (windowEndIndex - windowStartIndex);
		return avg * thresholdMultiplier;
	}

	float getPrunedSpectralFlux(int spectralFluxIndex) {
		return Mathf.Max (0f, spectralFluxSamples [spectralFluxIndex].spectralFlux - spectralFluxSamples [spectralFluxIndex].threshold);
	}

	bool isPeak(int spectralFluxIndex) {
		if (spectralFluxSamples [spectralFluxIndex].prunedSpectralFlux > spectralFluxSamples [spectralFluxIndex + 1].prunedSpectralFlux &&
			spectralFluxSamples [spectralFluxIndex].prunedSpectralFlux > spectralFluxSamples [spectralFluxIndex - 1].prunedSpectralFlux) {
			return true;
		} else {
			return false;
		}
	}

}