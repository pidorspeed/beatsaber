﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

using System.Numerics;
using DSPLib;


public class SongController : MonoBehaviour {

	float[] realTimeSpectrum;
	SpectralFluxAnalyzer realTimeSpectralFluxAnalyzer;
	PlotController realTimePlotController;

	int numChannels;
	int numTotalSamples;
	int sampleRate;
	float clipLength;
	float[] multiChannelSamples;
	SpectralFluxAnalyzer preProcessedSpectralFluxAnalyzer;
	PlotController preProcessedPlotController;

	AudioSource audioSource;

	public bool realTimeSamples = true;
	public bool preProcessSamples = false;

	void Start() {
		audioSource = GetComponent<AudioSource> ();

		if (preProcessSamples) {
			preProcessedSpectralFluxAnalyzer = new SpectralFluxAnalyzer ();
			preProcessedPlotController = GameObject.Find ("Spawn").GetComponent<PlotController> ();

			multiChannelSamples = new float[audioSource.clip.samples * audioSource.clip.channels];
			numChannels = audioSource.clip.channels;
			numTotalSamples = audioSource.clip.samples;
			clipLength = audioSource.clip.length;
			Debug.Log("total sempls" + numTotalSamples); // шоб знать скока сэмплав
			this.sampleRate = audioSource.clip.frequency;
			audioSource.clip.GetData(multiChannelSamples, 0);
                getFullSpectrumThreaded();
		}
	}

	void Update() {

		if (preProcessSamples) {
			int indexToPlot = getIndexFromTime (audioSource.time) / 1024;
			preProcessedPlotController.updatePlot (preProcessedSpectralFluxAnalyzer.spectralFluxSamples, indexToPlot);
            Debug.Log(" indexToPlot" + indexToPlot);
        }
       
	}

	public int getIndexFromTime(float curTime) {
		float lengthPerSample = this.clipLength / (float)this.numTotalSamples;

		return Mathf.FloorToInt (curTime / lengthPerSample);
	}

	public float getTimeFromIndex(int index) {
		return ((1f / (float)this.sampleRate) * index);
	}

	public void getFullSpectrumThreaded() {
		
			float[] preProcessedSamples = new float[this.numTotalSamples];

			int numProcessed = 0;
			float combinedChannelAverage = 0f;
			for (int i = 0; i < multiChannelSamples.Length; i++) {
				combinedChannelAverage += multiChannelSamples [i];

				
				if ((i + 1) % this.numChannels == 0) {
					preProcessedSamples[numProcessed] = combinedChannelAverage / this.numChannels;
					numProcessed++;
					combinedChannelAverage = 0f;
				}
			}

			Debug.Log ("каналы скомбинированы");
			Debug.Log (preProcessedSamples.Length);

			
			int spectrumSampleSize = 1024;
			int iterations = preProcessedSamples.Length / spectrumSampleSize;

			FFT fft = new FFT ();
			fft.Initialize ((UInt32)spectrumSampleSize);

			Debug.Log (string.Format("Processing {0} time domain samples for FFT", iterations));
			double[] sampleChunk = new double[spectrumSampleSize];
			for (int i = 0; i < iterations; i++) {


			  // Захватить 1024 фрагмента данных аудиосэмпла
				Array.Copy (preProcessedSamples, i * spectrumSampleSize, sampleChunk, 0, spectrumSampleSize);

			  // Применяем  FFT
			   double[] windowCoefs = DSP.Window.Coefficients (DSP.Window.Type.Hanning, (uint)spectrumSampleSize);
				double[] scaledSpectrumChunk = DSP.Math.Multiply (sampleChunk, windowCoefs);
				double scaleFactor = DSP.Window.ScaleFactor.Signal (windowCoefs);
				Complex[] fftSpectrum = fft.Execute (scaledSpectrumChunk);
				double[] scaledFFTSpectrum = DSPLib.DSP.ConvertComplex.ToMagnitude (fftSpectrum);
				scaledFFTSpectrum = DSP.Math.Multiply (scaledFFTSpectrum, scaleFactor);

			//шоб 1024 значения величины соответствовали(примерно) одной точке на временной шкале звука
				float curSongTime = getTimeFromIndex(i) * spectrumSampleSize;

				// отправляем в аналайз для исследования на бит
				preProcessedSpectralFluxAnalyzer.analyzeSpectrum (Array.ConvertAll (scaledFFTSpectrum, x => (float)x), curSongTime);
			}

        Debug.Log("Spectrum Analysis done");
        
    }
}